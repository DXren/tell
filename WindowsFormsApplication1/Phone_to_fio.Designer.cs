﻿namespace WindowsFormsApplication1
{
    partial class Phone_to_fio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Phone_PhDirecForm_ComBox = new System.Windows.Forms.ComboBox();
            this.FIO_PhDirecForm_ComBox = new System.Windows.Forms.ComboBox();
            this.PhDirecOK_BUTTON = new System.Windows.Forms.Button();
            this.PhDirecCancel_BUTTON = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.Phone_PhDirecForm_ComBox);
            this.groupBox1.Controls.Add(this.FIO_PhDirecForm_ComBox);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(233, 82);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Телефонный справочник";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(133, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Номер телефона";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(143, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "ФИО";
            // 
            // Phone_PhDirecForm_ComBox
            // 
            this.Phone_PhDirecForm_ComBox.FormattingEnabled = true;
            this.Phone_PhDirecForm_ComBox.Location = new System.Drawing.Point(6, 46);
            this.Phone_PhDirecForm_ComBox.Name = "Phone_PhDirecForm_ComBox";
            this.Phone_PhDirecForm_ComBox.Size = new System.Drawing.Size(121, 21);
            this.Phone_PhDirecForm_ComBox.TabIndex = 1;
            // 
            // FIO_PhDirecForm_ComBox
            // 
            this.FIO_PhDirecForm_ComBox.FormattingEnabled = true;
            this.FIO_PhDirecForm_ComBox.Location = new System.Drawing.Point(6, 19);
            this.FIO_PhDirecForm_ComBox.Name = "FIO_PhDirecForm_ComBox";
            this.FIO_PhDirecForm_ComBox.Size = new System.Drawing.Size(121, 21);
            this.FIO_PhDirecForm_ComBox.TabIndex = 0;
            // 
            // PhDirecOK_BUTTON
            // 
            this.PhDirecOK_BUTTON.Location = new System.Drawing.Point(12, 100);
            this.PhDirecOK_BUTTON.Name = "PhDirecOK_BUTTON";
            this.PhDirecOK_BUTTON.Size = new System.Drawing.Size(75, 23);
            this.PhDirecOK_BUTTON.TabIndex = 1;
            this.PhDirecOK_BUTTON.Text = "Применить";
            this.PhDirecOK_BUTTON.UseVisualStyleBackColor = true;
            this.PhDirecOK_BUTTON.Click += new System.EventHandler(this.PhDirecOK_BUTTON_Click);
            // 
            // PhDirecCancel_BUTTON
            // 
            this.PhDirecCancel_BUTTON.Location = new System.Drawing.Point(170, 100);
            this.PhDirecCancel_BUTTON.Name = "PhDirecCancel_BUTTON";
            this.PhDirecCancel_BUTTON.Size = new System.Drawing.Size(75, 23);
            this.PhDirecCancel_BUTTON.TabIndex = 2;
            this.PhDirecCancel_BUTTON.Text = "Отмена";
            this.PhDirecCancel_BUTTON.UseVisualStyleBackColor = true;
            this.PhDirecCancel_BUTTON.Click += new System.EventHandler(this.PhDirecCancel_BUTTON_Click);
            // 
            // Phone_to_fio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(256, 133);
            this.Controls.Add(this.PhDirecCancel_BUTTON);
            this.Controls.Add(this.PhDirecOK_BUTTON);
            this.Controls.Add(this.groupBox1);
            this.Name = "Phone_to_fio";
            this.Text = "Телефон и контакт";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.ComboBox Phone_PhDirecForm_ComBox;
        public System.Windows.Forms.ComboBox FIO_PhDirecForm_ComBox;
        public System.Windows.Forms.Button PhDirecOK_BUTTON;
        public System.Windows.Forms.Button PhDirecCancel_BUTTON;
    }
}